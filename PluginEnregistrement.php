<?php

/**
 * Plugin Name: PluginEnregistrement
 * Description: Plugin d'enregistrement des membres inscrits
 * Author: Virginie H.
 * Version: 1.0.0
 */

require_once plugin_dir_path(__FILE__).'Class_Wp_Widget_Registration.php';

class PluginEnregistrement {

    public function __construct() {
        register_activation_hook(__FILE__, array('Class_Wp_Widget_Registration', 'activate'));

        //vider les tables
        register_deactivation_hook(__FILE__, array('Class_Wp_Widget_Registration', 'deactivate') );

        //suppression table quand suppression plugin
        register_uninstall_hook(__FILE__, array('Class_Wp_Widget_Registration', 'uninstall'));

        add_action('widget_init', function(){
            register_widget('Class_Wp_Widget_Registration.php');
        });

        add_action('wp_enqueue_scripts', array($this, 'setting_link_script'));
        add_action('admin_menu', array($this, 'add_menu_back'));
    }

public function myClientBack() {
    echo "<H1>" .get_admin_page_title(). "</H1>";

    if(isset($_REQUEST['page']) == 'wp_clients') {
        $table = new Class_Wp_Liste_Clients();
        $table->prepare_items();
        echo $table->display();
    } else {
        echo "<form method='post'>",
        "<p>",
        "<label for='nom'>Nom :</label>",
        "<input type='text' id='nom' class='widefat'>",
        "</p>",
        "<p>",
        "<label for='adresse'>adresse :</label>",
        "<input type='text' id='adresse' class='widefat'>",
        "</p>",
        "<p>",
        "<label for='email'>Email :</label>",
        "<input type='text' id='email' class='widefat'>",
        "</p>",
        "<p>",
        "<label for='tel'>Téléphone :</label>",
        "<input type='text' id='tel' class='widefat'>",
        "</p>",
        "<p>",
        " <input type='submit' value='enregister' ",
        "</form>";
    }

    function saveMember()
    {
        global $wpdb;
        if(isset($_POST['email']) && !empty($_POST['email'])) {
            $nom = $_POST['nom'];
            $adresse = $_POST['adresse'];
            $email = $_POST['email'];
            $tel = $_POST['tel'];

            $row = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}adherent WHERE email = '".$email."';");

            if(is_null($row)){
                $wpdb->insert("{$wpdb->prefix}adherent", array(
                    'nom' => $nom,
                    'email' => $email,
                    'tel' => $tel,
                    'adresse' => $adresse
                ));
            }
        }
    }


    echo "<table class='widefat fixed' cellspacing='0'>
    <tr>
    <th class='manage-column column-columnename'>Id</th>
    <th class='manage-column column-columnename'>Nom</th>
    <th class='manage-column column-columnename'>Adresse</th>
    <th class='manage-column column-columnename'>Téléphone</th>
    </tr>
    </table>";

    foreach($ins->findAll() as $line) {
        echo '<tr>';
        echo '<th>';
        echo '</th>';
        echo '</td>';
    }
}
}


