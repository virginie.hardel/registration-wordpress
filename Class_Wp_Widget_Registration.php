<?php


class Class_Wp_Registration
{

    public function __construct()
    {
    }

    public static function activate()
    {
        global $wpdb;

        $wpdb->query('CREATE TABLE IF NOT EXISTS
            {$wpdb->prefix}adherents (
            id INT AUTO_INCREMENT PRIMARY KEY,
            nom VARCHAR(150) NOT NULL,
            prenom VARCHAR(255) NOT NULL,
            email VARCHAR(255) NOT NULL,
            tel INT,
            adresse VARCHAR(255),
            numero INT,
            club VARCHAR(255),
            specialite VARCHAR(150),
            points INT
        )');

        $count = $wpdb->get_var("SELECT count(*) from {$wpdb->prefix}wp_adherents;");

        if($count == 0){
            $wpdb->insert("{$wpdb->prefix}adherents", array(
                'id' => '',
               'nom' => 'Hardel',
               'prenom' => "Virginie",
               'email' => 'virginie.hardel@gmail.com',
               'tel' => '06 81 73 97 13',
               'adresse' => '2 allée du Niger, 31000, TOULOUSE',
               'numero' => '541468',
               'club' => '1',
               'specialite' => 'drone3',
               'points' => ''
            ));

            $wpdb->insert("{$wpdb->prefix}adherents", array(
                'id' => '',
                'nom' => 'Coutand',
                'prenom' => "Jenny",
                'email' => 'jenny.coutand@gmail.com',
                'tel' => '06 73 42 42 42',
                'adresse' => '3 impasse de l\'impasse, 42000 PréciserVille',
                'numero' => '416467',
                'club' => '2',
                'specialite' => 'automobile',
                'points' => '5'
             ));

             $wpdb->insert("{$wpdb->prefix}adherents", array(
                'id' => '',
                'nom' => 'Gangloff',
                'prenom' => "Romain",
                'email' => 'lapinou@gmail.com',
                'tel' => '06 66 66 66 66',
                'adresse' => '69 avenue du Bon samaritain',
                'numero' => '749416',
                'club' => '4',
                'specialite' => 'automobile',
                'points' => '3'
             ));
             $wpdb->insert("{$wpdb->prefix}adherents", array(
                'id' => '',
                'nom' => 'Gonzalez',
                'prenom' => "Florent",
                'email' => 'DarkSasukeDu97@gmail.com',
                'tel' => '09 11 22 33 44',
                'adresse' => '18 rue de Motorbike',
                'numero' => '44826',
                'club' => '3',
                'specialite' => 'drone4',
                'points' => ''
             ));

        }

    }

    // function lors de la désactivation
    function deactivate()
    {
        global $wpdb;
        $wpdb->query("TRUNCATE {$wpdb->prefix}.'cmio_member';");
    }

    // function lors de la suppression du plugin
    function uninstall()
    {
        global $wpdb;
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}.'cmio_member';");
    }

    function findAll()
    {
        global $wpdb;
        $res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}. 'cmio_member';", ARRAY_A);
        return $res;
    }

}


class Class_Wp_Club_Registration
{

    public function __construct()
    {
    }

    public static function activate()
    {
        global $wpdb;


        $wpdb->query('CREATE TABLE IF NOT EXISTS
            {$wpdb->prefix}club
            ( `id` INT(11) NOT NULL AUTO_INCREMENT,
            `nom` VARCHAR(50) NOT NULL,
            `adresse` VARCHAR(50) NULL DEFAULT NULL,
            `tel` VARCHAR(50) NULL DEFAULT NULL,
            `domaine` VARCHAR(50) NULL DEFAULT NULL,
            PRIMARY KEY (`id`)
            )');

        $count = $wpdb->get_var('SELECT count(*) from {$wpdb->prefix}club;');

        if($count == 0){
            $wpdb->insert("{$wpdb->prefix}club", array(
            'id' => '',
            'nom' => 'Champagne',
            'adresse' => '42 avenue de la prise de tete sur wordpress',
            'tel' => '06 81 73 97 13',
            'domaine' => 'drones3',
            ));

            $wpdb->insert("{$wpdb->prefix}club", array(
            'id' => '',
            'nom' => 'Rosé',
            'adresse' => 'Ceci est une superbe adresse',
            'tel' => '06 81 73 97 13',
            'domaine' => 'automobile',
            ));

            $wpdb->insert("{$wpdb->prefix}club", array(
            'id' => '',
            'nom' => 'Piquette',
            'adresse' => 'L\'originalité s\'est fait la malle, laissez-moi tranquille',
            'tel' => '06 81 73 97 13',
            'domaine' => 'drones4',
            ));

            $wpdb->insert("{$wpdb->prefix}club", array(
            'id' => '',
            'nom' => 'Whisky',
            'adresse' => 'Blabla blablabla',
            'tel' => '06 81 73 97 13',
            'domaine' => 'automobile',
            ));
        }
    }
    // function lors de la désactivation
    function deactivate()
    {
        global $wpdb;
        $wpdb->query("TRUNCATE {$wpdb->prefix}.'cmio_member';");
    }

    // function lors de la suppression du plugin
    function uninstall()
    {
        global $wpdb;
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}.'cmio_member';");
    }

    function findAll()
    {
        global $wpdb;
        $res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}. 'cmio_member';", ARRAY_A);
        return $res;
    }

}

