<?php

// Classe d'enregistrement de nouveaux membres

class Class_Wp_Registration
{

    public function __construct()
    {
    }

    public static function install()
    {
        global $wpdb;
        $wpdb->query("CREATE TABLE IF NOT EXISTS " .
            "{$wpdb->prefix}member ( " .
            "id INT AUTO_INCREMENT PRIMARY KEY," .
            " nom VARCHAR(150) NOT NULL," .
            " prenom VARCHAR(255) NOT NULL," .
            " tel VARCHAR(255)," .
            " email VARCHAR(255) NOT NULL," .
            " adresse VARCHAR(255);");

        $count = $wpdb->get_var("SELECT count(*) FROM {$wpdb->prefix}member;");

        if ($count == 0) {
            $wpdb->insert("{$wpdb->prefix}member", array(
                'nom' => 'Choucroute',
                'prenom' => "Tartiflette",
                'tel' => '000000000',
                'email' => 'raclette@gmail.com',
                'adresse' => '78 avenue du fromage en croûtes'
            ));
        }
    }

    public static function deactivate()
    {
        global $wpdb;
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}member;");
    }

    public static function uninstall()
    {
        global $wpdb;
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}member;");
    }

    /**
     * lister tout les membres
     * @return array|object|null
     */
    public function findAll()
    {
        global $wpdb;
        $all = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}member;", ARRAY_A);
        return $all;
    }

    public function saveMember()
    {
        global $wpdb;
        if (isset($_POST['email']) && !empty($_POST['email'])) {
            $nom = $_POST['nom'];
            $prenom = $_POST['prenom'];
            $tel = $_POST['tel'];
            $email = $_POST['email'];
            $adresse = $_POST['adresse'];

            $row = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}member WHERE email = '" . $email . "';");

            if (is_null($row)) {
                $wpdb->insert("{$wpdb->prefix}member", array(
                    'nom' => $nom,
                    'prenom' => $prenom,
                    'tel' => $tel,
                    'email' => $email,
                    'adresse' => $adresse
                ));
            }
        }
    }

    public function deleteById($ids)
    {
        if (!is_array($ids)) { // si $ids n'est pas un tableau
            $ids = array($ids); // on crée le tableau à une valeur
        }

        global $wpdb;

        $wpdb->query("DELETE from {$wpdb->prefix}member WHERE id IN (" . implode(',', $ids) . ");");
    }
}

